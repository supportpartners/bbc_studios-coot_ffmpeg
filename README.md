# BBC_Studios - COOT_FFMPEG
## Batch Files
* ffmpeg-join-master.bat
    * Contains all ffmpeg commands

* ffmpeg-join-1.bat
    * Contains two ffmpeg commands allowing for running in conjunction with other numbered ffmpeg-join batch files.

* ffmpeg-join-2.bat
    * Contains two ffmpeg commands allowing for running in conjunction with other numbered ffmpeg-join batch files.

* ffmpeg-join-3.bat
    * Contains two ffmpeg commands allowing for running in conjunction with other numbered ffmpeg-join batch files.

* ffmpeg-join-4.bat
    * Contains two ffmpeg commands allowing for running in conjunction with other numbered ffmpeg-join batch files.

* ffmpeg-join-5.bat
    * Contains two ffmpeg commands allowing for running in conjunction with other numbered ffmpeg-join batch files.
